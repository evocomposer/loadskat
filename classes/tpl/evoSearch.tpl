<div class="row news-item" style="margin-top: 0px !important;">
	<div class="col-lg-9 col-md-8 col-sm-9 col-xs-8 pull-right news-item-title">
		<h4 class="search-item-title"><a href="[+url+]">[+pagetitle+]</a></h4>
	</div>
	<div class="col-lg-3 col-md-4 col-sm-3 col-xs-4 news-item-image">
		<a href="[+url+]" style="background-image: url('/[[phpthumb? &input=`[+tv.ogimage+]` &options=`w=648,h=307,far=C`]]');"></a>
	</div>
	<div class="col-lg-9 col-md-8 col-sm-9 col-xs-8 pull-right news-item-description">
		<div class="search-item-extract">[+extract+]</div>
	</div>
</div>