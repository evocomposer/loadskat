{{pixel_coockie}}
<footer class="footer container-fluid">
	<div class="row">
		<div class="container">
			<div class="row footer-wrapper mt-35">
				<div class="col-lg-4 col-md-4 footer-wrapper-left"><span><a href=""><img src="/assets/templates/skat_1.0.0/images/logotip.png" class="img-respomsive"><span>[(site_name)]</span></a></span></div>
				<!-- [(postalCode)], [(addressLocality)], [(streetAddress)] -->
				<div class="col-lg-4 col-md-4 footer-wrapper-center text-center">[(client_infoaddress)]<br>[(telephone)]</div>
				<div class="col-lg-4 col-md-4 footer-wrapper-right"><button class="btn btn-zayavka text-uppercase techic">Заявка на технику</button><button class="btn btn-zayavka text-uppercase zapchast">Заявка на запчасти</button></div>
				<div class="col-xs-12 footer-wrapper-copyright text-center clearfix">
					<ul class="copyright-menu mt-20">
						<li><a href="[~29~]">Контакты</a></li>
						<li><a href="[~443~]">Написать нам</a></li>
						<li><a href="[~493~]">Карта сайта</a></li>
					</ul>
					<div class="copyright-wrapper"><span>Все права защищены<br>[(company_copyright)]<br>Разработка и поддержка: <a href="http://www.cmsmagazine.ru/creators/studionions/" target="_blank">ProjectSoft & STUDIONIONS</a></span></div>
					<!--div class="text-center copyright-wrapper">Memory: [^m^], MySQL: [^qt^], [^q^] request(s), PHP: [^p^], total: [^t^], document retrieved from [^s^].</div-->
				</div>
			</div>
		</div>
	</div>
</footer>
<!--yepnope.2.0.0.js-->
[!#GetFileContent? &input=`assets/templates/skat_1.0.0/js/yepnope.2.0.0.js` &type=`js`!]
<!--modernizr.js-->
[!#GetFileContent? &input=`assets/templates/skat_1.0.0/js/modernizr.js` &type=`js`!]
<!--load-->
<script>
	window.googleKey = '[(googleKey)]';
	yepnope('[!#GetFileAndTime? &input=`assets/templates/skat_1.0.0/js/app.js`!]', undefined, function() {
		yepnope('[!#GetFileAndTime? &input=`assets/templates/skat_1.0.0/js/hypher.js`!]', undefined, function() {
			yepnope('[!#GetFileAndTime? &input=`assets/templates/skat_1.0.0/js/main.js`!]', undefined, function(){
				window.skat = {
					zayavka: (function(){
						return false;
					})()
				};
				
				(function (d, w, c) {
					(w[c] = w[c] || []).push(function() {
						try {
							w.yaCounter22079803 = new Ya.Metrika2({
								id:22079803,
								clickmap:true,
								trackLinks:true,
								accurateTrackBounce:true,
								webvisor:true,
								ecommerce:"dataLayer"
							});
						} catch(e) { }
					});

					var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
					s.type = "text/javascript";
					s.async = true;
					s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js";

					if (w.opera == "[object Opera]") {
						d.addEventListener("DOMContentLoaded", f, false);
					} else { f(); }
				})(document, window, "yandex_metrika_callbacks2");
				
				yepnope('https://apis.google.com/js/platform.js?publisherid=117835748354355612092', undefined, function(){
					(function(i,s,o,g,r,a,m){
						i['GoogleAnalyticsObject']=r;
						i[r]=i[r]||function(){
							(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
					ga('create', 'UA-43229772-1', 'skat59.ru');
					ga('send', 'pageview');
				});
				{{pixel_js}}
			})
		});
	});
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22079803" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- end load -->
{{#forms}}