<head>
	<meta http-equiv="Content-Type" content="text/html; charset=[(modx_charset)]" />
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
	<meta name="cmsmagazine" content="d8e7426ec72ad3e4ea38b09ebf01284c">
	<meta name='yandex-verification' content='6d68459166068c11'>
	<title>[*titl*]</title>
	[*noIndex*]
	<base href="[(site_url)]"/>
	<!--link href="[(site_url)]<@IF:[*id:isnt(1)*]>[~[*id*]~]<@ENDIF>" rel="canonical"-->
	{{#favicons}}
	<meta name="title" content="[*titl:hsc*]">
	<meta name="keywords" content="[*keyw:hsc*]" />
	<meta name="description" content="[*desc:hsc*]" />
    <meta property="og:title" content="[*titl:hsc*]">
    <meta property="og:description" content="[*desc:hsc*]">
    <meta property="og:url" content="[(site_url)]<@IF:[*id:isnt(1)*]>[~[*id*]~]<@ENDIF>">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="[(companyName:hsc)]">
	<meta itemprop="image" content="[(site_url)][*ogimage*]">
	[!#OgImage!]
	<script>
	window.onload = function(){
		setTimeout(function(){
			$("body").removeClass("preload")
		}, 1500);
	}
	</script>
	[!#GetFileContent? &input=`assets/templates/skat_1.0.0/css/main.css` &type=`css`!]
</head>