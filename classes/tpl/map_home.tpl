<section class="container-fluid map-home">
	<div class="row">
		<div class="container">
			<h3 class="section_h2 text-center text-uppercase mb-30 mt-30 title-line-black">Как к нам проехать</h3>
		</div>
	</div>
	<div class="row embed-responsive data-map">
		<div id="map" data-latitude="[(latitude)]" data-longitude="[(longitude)]" data-zoom="[(zoom_map)]" data-title="[(site_name)]" data-addr="[(client_mapsearch)]" data-address="[(postalCode)], [(addressLocality)], [(streetAddress)]" data-duration="ПН — ПТ с 9.00 до 18.00" data-phone="[(telephone)]" data-marker="/assets/images/marker.png" data-animation="2" class="embed-responsive-item"></div>
	</div>
</section>