<div class="container-fluid breadcrumbs">
	<div class="row">
		<div class="container">
			[[bread_crumbs? &templateSet=`schemaBreadcrumbList`]]
		</div>
	</div>
</div>
[[Wayfinder? &config=`parent`]]